/* uSlide
 * Copyright (c) 2012-2013 Dominik Marczuk
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * The name of Dominik Marczuk may not be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY DOMINIK MARCZUK "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DOMINIK MARCZUK BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

(function($) {
	var uSlide = function (target, options) {
		/* CONFIGURATION */

		options = $.extend({
			// general animation info
			animation: "slide",
			delay: 2000,
			easing: "swing",
			framerate: 50,
			speed: 500,
			// detailed animation info
			nextFadeInAnimation: null,
			nextFadeInDirection: "right",
			nextFadeInEasing: null,
			nextFadeInSpeed: null,
			nextFadeOutAnimation: null,
			nextFadeOutDirection: "left",
			nextFadeOutEasing: null,
			nextFadeOutSpeed: null,
			prevFadeInAnimation: null,
			prevFadeInDirection: "left",
			prevFadeInEasing: null,
			prevFadeInSpeed: null,
			prevFadeOutAnimation: null,
			prevFadeOutDirection: "right",
			prevFadeOutEasing: null,
			prevFadeOutSpeed: null,
			// custom animation CSS
			customCss: false,
			css: {
				current: null,
				nextFadeIn: null,
				nextFadeOut: null,
				prevFadeIn: null,
				prevFadeOut: null
			},
			// custom animate functions
			customAnimate: false,
			animate: {
				nextFadeIn: null,
				nextFadeOut: null,
				prevFadeIn: null,
				prevFadeOut: null
			},
			// playing options
			autoStart: true,
			pauseOnHover: true,
			swipe: ("ontouchend" in document),
			// additional elements
			navigator: ".navigator",
			next: ".next",
			prev: ".prev",
			progress: ".progress",
			slider: ".slider"
		}, options);

		// set custom CSS/animation use
		if (options.css.current != null
			&& options.css.nextFadeIn != null
			&& options.css.nextFadeOut != null
			&& options.css.prevFadeIn != null
			&& options.css.prevFadeOut != null) {
			options.customCss = true;
			options.customAnimate = false;
		} else if (options.animate.nextFadeIn != null
			&& options.animate.nextFadeOut != null
			&& options.animate.prevFadeIn != null
			&& options.animate.prevFadeOut != null) {
			options.customCss = false;
			options.customAnimate = true;
		} else {
			options.customCss = false;
			options.customAnimate = false;
		}

		// necessary variables
		var
			me = this,

			index = 0,
			slides = [],
			originalStyles = [],
			navigatorItems = [],

			isPlaying = false,
			progress = 0,
			elapsed = 0,

			prev = target.find(options.prev),
			next = target.find(options.next),
			navigator = target.find(options.navigator),
			progressBar = target.find(options.progress),

			// this one is used for marking whether the slider is hovered over
			hover = false;


		/* PRIVATE FUNCTIONS */

		/**
		 * Get source/target CSS of a slide animation
		 *
		 * @param animation animation type ("slide", "fade", "slide fade")
		 * @param direction animation direction ("top", "left", "bottom right", etc.)
		 *
		 * @return {Object}
		 */
		var getCss = function (animation, direction) {
			var css = {};

			if (null === animation) {
				animation = options.animation;
			}

			animation = animation.split(" ");
			direction = direction.split(" ");


			if ($.inArray("fade",animation) > -1) {
				css.opacity = 0;
			} else {
				css.opacity = 1;
			}

			if ($.inArray("slide",animation) > -1) {
				if ($.inArray("top",direction) > -1) {
					css.top = -target.innerHeight() + "px";
				}
				if ($.inArray("bottom",direction) > -1) {
					css.top = target.innerHeight() + "px";
				}
				if ($.inArray("left",direction) > -1) {
					css.left = -target.innerWidth() + "px";
				}
				if ($.inArray("right",direction) > -1) {
					css.left = target.innerWidth() + "px";
				}
			} else {
				css.top = 0;
				css.left = 0;
			}

			return css;
		};

		/**
		 * Get the current time in milliseconds
		 *
		 * @return {Number}
		 */
		var getMilliSeconds = function () {
			return new Date().getTime();
		};

		/**
		 * Update the navigator(s) attached to a slider.
		 */
		var updateNavigator = function () {
			navigator.find(".item").removeClass("active");
			navigator.find(".item:nth-child("+(index+1)+")").addClass("active");
		};

		/* PUBLIC API */

		/**
		 * Get currently displayed slide
		 *
		 * @return {Object} jQuery object correcponding to the currently displayed slide
		 */
		this.getCurrentSlide = function () {
			return slides[index];
		};

		/**
		 * Get the currently displayed image's index
		 *
		 * @return {Number}
		 */
		this.getIndex = function () {
			return index;
		};

		/**
		 * Get the total number of slides
		 *
		 * @return {Number}
		 */
		this.getSlideCount = function () {
			return slides.length;
		};

		/**
		 * Get next slide
		 *
		 * @return {Object} a jQuery object corresponding to the next slide
		 */
		this.getNextSlide = function () {
			return slides[(index + 1) % slides.length];
		};

		/**
		 * Get previous slide
		 *
		 * @return {Object} a jQuery object corresponding to the previous slide
		 */
		this.getPreviousSlide = function () {
			return slides[(index + slides.length - 1) % slides.length];
		};

		/**
		 * Get a slide
		 *
		 * @param idx Slide index (0-based)
		 *
		 * @return {Object} a jQuery object corresponding to the requested slide
		 */
		this.getSlide = function (idx) {
			return slides[idx];
		};

		/**
		 * Get slide that's a given number of slides before or after the current one
		 *
		 * @param offset Slide index offset in relation to the current one (e.g. -1 will fetch the previous slide)
		 *
		 * @return {Object} a jQuery object corresponding to the requested slide
		 */
		this.getSlideRelative = function (offset) {
			while (index + offset < 0) {
				offset += slides.length;
			}
			return slides[(index + offset) % slides.length];
		};

		/**
		 * Slide to the requested slide number using the "next" animation
		 *
		 * @param idx Slide index
		 *
		 * @return {Boolean} always false to prevent default next button behaviour
		 */
		this.next = function (idx) {
			if (typeof idx != "number") {
				idx = index + 1;
			}
			while (idx < 0) {
				idx += slides.length;
			}
			idx %= slides.length;
			if (idx == index) {
				return false;
			}

			progress = 0;

			var nextSlide = slides[idx].stop(true,false);
			var currentSlide = slides[index].stop(true,false);

			// apply custom animations
			if (options.customAnimate) {
				options.animate.nextFadeIn(nextSlide);
				options.animate.nextFadeOut(currentSlide);
			} else {
				var fadeInCss, fadeOutCss;

				if (options.customCss) {
					nextSlide.css(options.css.nextFadeIn);
					fadeInCss = options.css.current;
					currentSlide.css(options.css.current);
					fadeOutCss = options.css.nextFadeOut;
				} else {
					nextSlide.css(getCss(options.nextFadeInAnimation, options.nextFadeInDirection));
					fadeInCss = {top: 0, left: 0, opacity: 1};
					currentSlide.css(fadeInCss);
					fadeOutCss = getCss(options.nextFadeOutAnimation, options.nextFadeOutDirection);
				}

				var fadeInSpeed = options.nextFadeInSpeed != null ? options.nextFadeInSpeed : options.speed;
				var fadeOutSpeed = options.nextFadeOutSpeed != null ? options.nextFadeOutSpeed : options.speed;

				var fadeInEasing = options.nextFadeInEasing != null ? options.nextFadeInEasing : options.easing;
				var fadeOutEasing = options.nextFadeOutEasing != null ? options.nextFadeOutEasing : options.easing;

				nextSlide.trigger("uSlide.nextFadeInStart",[me]);
				currentSlide.trigger("uSlide.nextFadeOutStart",[me]);

				if ("transition" in $.fn && options.customCss) {
					nextSlide.show().transition(fadeInCss, fadeInSpeed, fadeInEasing, function () { nextSlide.trigger("uSlide.nextFadeInEnd",[this]); });
					currentSlide.transition(fadeOutCss, fadeOutSpeed, fadeOutEasing, function () { currentSlide.hide().trigger("uSlide.nextFadeOutEnd",[this]); });
				} else {
					nextSlide.show().animate(fadeInCss, fadeInSpeed, fadeInEasing, function () { nextSlide.trigger("uSlide.nextFadeInEnd",[this]); });
					currentSlide.animate(fadeOutCss, fadeOutSpeed, fadeOutEasing, function () { currentSlide.hide().trigger("uSlide.nextFadeOutEnd",[this]); });
				}
			}

			index = idx;
			updateNavigator();

			return false;
		};

		/**
		 * Slide to the requested slide number using the "previous" animation
		 *
		 * @param idx Slide index
		 *
		 * @return {Boolean} always false to prevent default next button behaviour
		 */
		this.previous = function (idx) {
			if (typeof idx != "number") {
				idx = index - 1;
			}
			while (idx <= 0) {
				idx += slides.length;
			}
			idx %= slides.length;
			if (idx == index) {
				return false;
			}

			progress = 0;

			var previousSlide = slides[idx].stop(true,false);
			var currentSlide = slides[index].stop(true,false);

			// apply custom animations
			if (options.customAnimate) {
				options.animate.prevFadeOut(currentSlide);
				options.animate.prevFadeIn(previousSlide);
			} else {
				var fadeInCss, fadeOutCss;

				if (options.customCss) {
					previousSlide.css(options.css.prevFadeIn);
					fadeInCss = options.css.current;
					currentSlide.css(options.css.current);
					fadeOutCss = options.css.prevFadeOut;
				} else {
					previousSlide.css(getCss(options.prevFadeInAnimation, options.prevFadeInDirection));
					fadeInCss = {top: 0, left: 0, opacity: 1};
					currentSlide.css({top: 0, left: 0, opacity: 1});
					fadeOutCss = getCss(options.prevFadeOutAnimation, options.prevFadeOutDirection);
				}

				var fadeInSpeed = options.prevFadeInSpeed != null ? options.prevFadeInSpeed : options.speed;
				var fadeOutSpeed = options.prevFadeOutSpeed != null ? options.prevFadeOutSpeed : options.speed;

				var fadeInEasing = options.prevFadeInEasing != null ? options.prevFadeInEasing : options.easing;
				var fadeOutEasing = options.prevFadeOutEasing != null ? options.prevFadeOutEasing : options.easing;

				previousSlide.trigger("uSlide.prevFadeInStart",[me]);
				currentSlide.trigger("uSlide.prevFadeOutStart",[me]);

				if ("transition" in $.fn && options.customCss) {
					previousSlide.show().transition(fadeInCss, fadeInSpeed, fadeInEasing, function () { previousSlide.trigger("uSlide.prevFadeInEnd",[this]); });
					currentSlide.transition(fadeOutCss, fadeOutSpeed, fadeOutEasing, function () { currentSlide.hide().trigger("uSlide.prevFadeOutEnd",[this]); });
				} else {
					previousSlide.show().animate(fadeInCss, fadeInSpeed, fadeInEasing, function () { previousSlide.trigger("uSlide.prevFadeInEnd",[this]); });
					currentSlide.animate(fadeOutCss, fadeOutSpeed, fadeOutEasing, function () { currentSlide.hide().trigger("uSlide.prevFadeOutEnd",[this]); });
				}
			}

			index = idx;
			updateNavigator();

			return false;
		};

		/**
		 * Slide to the requested slide number. Animation type is resolved automatically, but may be forced.
		 *
		 * @param idx Slide index
		 * @param type Animation type: "next" or "previous". If omitted (default), the animation will be determined automatically.
		 */
		this.slideTo = function (idx, type) {
			if (typeof idx != "number") {
				throw "SlideTo expects a number parametre.";
			}

			while (idx < 0) {
				idx += slides.length;
			}
			idx %= slides.length;

			if (type == "next") {
				me.next(idx);
			} else if (type == "previous" || type == "prev") {
				me.previous(idx);
			} else {
				if (idx > index) {
					me.next(idx);
				} else if (idx < index) {
					me.previous(idx);
				}
			}
		};

		/**
		 * Play the slider
		 */
		this.play = function () {
			isPlaying = true;

			elapsed = getMilliSeconds();
			setTimeout(function () {
				if (options.pauseOnHover && hover) {
					progress = 0;
				} else {
					progress += getMilliSeconds() - elapsed;
				}

				if (progress >= options.delay) {
					me.next();
				}

				if (isPlaying) {
					me.play();
				}
			}, options.framerate);
		};

		/**
		 * Stop the slider
		 */
		this.stop = function () {
			isPlaying = false;
			progress = 0;
		};

		/**
		 * Pause the slider
		 */
		this.pause = function () {
			isPlaying = false;
		};

		/**
		 * Get the progress of the current slide's delay.
		 *
		 * @return {Number} a floating point number in the range <0-1>
		 */
		this.getProgress = function () {
			return progress / options.delay;
		};


		/* OBJECT INITIALISATION */

		// apply CSS to the slider, extract slides, bind hover events
		target
			.find(options.slider)
			.css({ position: "relative" })
			.children().each(function(idx, elt) {
				$(elt).css({ position: "absolute" });
				slides.push($(elt));
				originalStyles.push($(elt).attr("style"));
			})
			.bind({
				mouseenter: function () { hover = true; },
				mouseleave: function () { hover = false; }
			});

		// apply swipe gestures (if applicable)
		if (options.swipe) {
			var touch = ("ontouchend" in document);
			var startEvent = touch ? "touchstart" : "mousedown";
			var moveEvent = touch ? "touchmove" : "mousemove";
			var endEvent = touch ? "touchend" : "mouseup";
			var maxTime = 1000, maxDistance = 50;
			var startX = 0, startTime = 0;

			target
				.bind(startEvent, function(e) {
					e.preventDefault();
					startTime = e.timeStamp;
					startX = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
				})
				.bind(endEvent, function() {
					startTime = 0;
					startX = 0;
				})
				.bind(moveEvent, function (e) {
					e.preventDefault();
					var currentX = e.originalEvent.touches ? e.originalEvent.touches[0].pageX : e.pageX;
					var currentDistance = (startX) ? Math.abs(currentX - startX) : 0;
					var currentTime = e.timeStamp - startTime;
					if (startTime && currentTime <= maxTime && currentDistance >= maxDistance) {
						if (currentX < startX) {
							me.next();
						} else if (currentX > startX) {
							me.previous();
						}
						startTime = 0;
						startX = 0;
					}
				});
		}

		// set appropriate CSS
		for (var i = 1; i < slides.length; ++i) {
			if (options.customCss) {
				slides[i].css(options.css.nextFadeOut);
			} else {
				slides[i].css(getCss(options.nextFadeOutAnimation, options.nextFadeOutDirection));
			}
		}

		// create buttons
		if (prev.length > 0) {
			for (i = 0; i < prev.length; ++i) {
				prev[i].onclick = me.previous;
			}

		}
		if (next.length > 0) {
			for (i = 0; i < next.length; ++i) {
				next[i].onclick = me.next;
			}
		}

		// create the navigator
		if (navigator.length > 0) {
			for (i = 0; i < slides.length; ++i) {
				var item = $('<a href="#" class="item" rel="'+i+'">'+(i+1)+'</a>');
				item.bind({
					click: function (e) {
						e.preventDefault();
						me.slideTo(parseInt(this.rel));
					}
				});
				navigatorItems.push(item);
			}
			navigatorItems[0].addClass("active");
			for (var j = 0; j < navigatorItems.length; ++j) {
				navigator.append(navigatorItems[j]);
			}
		}

		// take care of the progress bar
		if (progressBar.length > 0) {
			setInterval(function () {
				progressBar.each(function() {
					$(this).css({ width: (me.getProgress() * 100) + "%" });
				});
			}, options.framerate);
		}

		// play the sliders
		if (options.autoStart) {
			this.play();
		}
	};

	$.fn.extend({
		/**
		 * Fetch (and create, if necessary) a uSlide object
		 *
		 * @return uSlide object
		 */
		uSlide: function () {
			var args = arguments;

			this.each(function() {
				if ($(this).data("uSlide") == null) {
					$(this).data("uSlide", new uSlide($(this), args.length && typeof args[0] == "object" ? args[0] : {}));
				}
			});

			return this.data("uSlide");
		}
	});
})(jQuery);
